from json.decoder import JSONDecodeError
import requests
import os

if not os.path.exists("./省级"):
    os.mkdir("./省级")
if not os.path.exists("./市级"):
    os.mkdir("./市级")
if not os.path.exists("./县级"):
    os.mkdir("./县级")

headers = {
    "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
    "Accept-Encoding": "gzip, deflate",
    "Accept-Language": "zh-CN,zh;q=0.9",
    "Cache-Control": "max-age=0",
    "User-Agent": "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"
}

url = "https://geo.datav.aliyun.com/areas_v3/bound/100000_full.json"
resp = requests.get(url=url, headers=headers, timeout=60)
respText = resp.text
respJson = resp.json()
# 写入全国地图
f = open("./{}.json".format("中华人民共和国"), 'w', encoding="utf-8")
f.write(str(respText))

# 遍历市州
provinces = respJson["features"]
for province in provinces:
    adcodeProvince = province["properties"]["adcode"]
    nameProvince = province["properties"]["name"]
    if nameProvince == "":
        continue
    print("省级-", adcodeProvince, "-", nameProvince)
    url = "https://geo.datav.aliyun.com/areas_v3/bound/{}_full.json".format(adcodeProvince)
    try:
        resp = requests.get(url=url, headers=headers, timeout=60)
        respText = resp.text
        respJson = resp.json()
        full = True
    except JSONDecodeError:
        url = "https://geo.datav.aliyun.com/areas_v3/bound/{}.json".format(adcodeProvince)
        resp = requests.get(url=url, headers=headers, timeout=60)
        respText = resp.text
        respJson = resp.json()
        full = False
    # 写入省级地图
    f = open("./省级/{}.json".format(nameProvince), 'w', encoding="utf-8")
    f.write(str(respText))
    if not full:
        continue

    # 遍历市州
    cities = respJson["features"]
    for city in cities:
        adcodeCity = city["properties"]["adcode"]
        nameCity = city["properties"]["name"]
        print("市级-", adcodeCity, "-", nameCity)
        url = "https://geo.datav.aliyun.com/areas_v3/bound/{}_full.json".format(adcodeCity)
        try:
            resp = requests.get(url=url, headers=headers, timeout=60)
            respText = resp.text
            respJson = resp.json()
            full = True
        except JSONDecodeError:
            url = "https://geo.datav.aliyun.com/areas_v3/bound/{}.json".format(adcodeCity)
            resp = requests.get(url=url, headers=headers, timeout=60)
            respText = resp.text
            respJson = resp.json()
            full = False
        # 写入市级地图
        f = open("./市级/{}.json".format(nameCity), 'w', encoding="utf-8")
        f.write(str(respText))

        if not full:
            continue

        # 遍历区县
        counties = respJson["features"]
        for country in counties:
            adcodeCountry = country["properties"]["adcode"]
            nameCountry = country["properties"]["name"]
            print("县级-", adcodeCountry, "-", nameCountry)
            url = "https://geo.datav.aliyun.com/areas_v3/bound/{}_full.json".format(adcodeCountry)
            try:
                resp = requests.get(url=url, headers=headers, timeout=60)
                respText = resp.text
                respJson = resp.json()
                full = True
            except JSONDecodeError:
                url = "https://geo.datav.aliyun.com/areas_v3/bound/{}.json".format(adcodeCountry)
                resp = requests.get(url=url, headers=headers, timeout=60)
                respText = resp.text
                respJson = resp.json()
                full = False
            # 写入县级地图
            f = open("./县级/{}.json".format(nameCountry), 'w', encoding="utf-8")
            f.write(str(respText))
